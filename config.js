const qs = new URLSearchParams(window.location.search);

export const tileset = qs.get('set') || 'celadon';
export const seed = qs.has('seed') ? parseInt(qs.get('seed')) : new Date().getTime();
export const showDebug = qs.get('debug') || false;
export const slow = qs.get('slow') || false;
export const pause = qs.get('pause') || false;
export const showGuesses = qs.get('showGuessTiles') || false;
export const chunkWidth = (qs.has('width') && Number.isInteger(Number(qs.get('width')))) ? Number(qs.get('width')) : 64;
export const chunkHeight = (qs.has('height') && Number.isInteger(Number(qs.get('height')))) ? Number(qs.get('height')) : 64;
export const autoReload = qs.has('autoReload');

export const keyMap = {
	'ArrowLeft':  'left',
	'ArrowRight': 'right',
	'ArrowUp':    'up',
	'ArrowDown':  'down',
	'Escape':     'pause',
	'=':          'increaseSpeed',
	'-':          'decreaseSpeed',
	'0':          'resetSpeed'
};
