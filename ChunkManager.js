class ChunkManager
{
	let seed = 123;
	let seedString = "";
	let seedSelected = false;
	let chunks = [];
	let radius = 0.0;
	let centerPos = { x: 0, y: 0 };

	let loop = 15;
	let cDist = 2;
	let chunkSize = { x: 30, y: 30 };

	let lastFrame;
	let crc;

	let chunksPerFrame = 1;
	let buff = 1;
	let gen;
	let player;
	let startHeight = 0;

	let rSpeed = 30;


	function update() {
		if (seedSelected) 
		{
			spawnChunks();
			moveCenter();
		}

	}



	function spawnChunks()
	{	
		let posx = centerPos.x/chunkSize.x;
		let posz = centerPos.y/chunkSize.z;
		let cg = canGen();
		let count = 0;
		for(let y = posy-loop; (y < posy+loop) && cg; y++) 
		{
			for(let x = posx-loop; (x < posx+loop) && cg; x++) 
			{
				let p = { x: x*chunkSize.x, y: y*chunkSize.y);
				if (cg && (Vector2.Distance(p,centerPos)<=radius)) 
				{
					if (chunks[count]!=null) 
					{
						let pos:Vector3 = chunks[count].position;
						let m:Mesh = chunks[count].GetComponent(MeshFilter).mesh;
						let c:Mesh = chunks[count].GetComponent(MeshCollider).sharedMesh;
						if (((pos.x<=(chunkSize.x*cDist+centerPos.x))&&(pos.z<=(chunkSize.z*cDist+centerPos.y))&&(pos.x>=(-chunkSize.x*cDist+centerPos.x))&&(pos.z>=(-chunkSize.z*cDist+centerPos.y)))&&c!=m) 
						{
							chunks[count].GetComponent(MeshCollider).sharedMesh = m;
							cg = canGen();
						}
						m=null;
						c=null;
					}
					else 
					{
						chunks[count] = Instantiate (chunk, Vector3(x*chunkSize.x, 0, y*chunkSize.z), Quaternion.identity);
						gen.generateChunk(chunks[count].position,chunks[count].GetComponent(MeshFilter).mesh,chunkSize,seed);
						cg = canGen();
					} 
				}


				count++;
			}
		}
		if (cg) 
		{
			radius += rSpeed;
		}
	}

	function moveCenter() 
	{	
		let dx = transform.position.x - centerPos.x;
		let dy = transform.position.z - centerPos.y;
		if(Mathf.Abs(dx) < chunkSize.x) dx = 0; else if(dx < 0) dx = -1; else dx = 1;
		if(Mathf.Abs(dy) < chunkSize.z) dy = 0; else if(dy < 0) dy = -1; else dy = 1;
		if(dx == 0 && dy == 0) return;
		let offset = dy * loop * 2 + dx;
		let width:int = loop*2;
		let size:int = width*width;
		let start:int = 0;
		let end:int = size;
		let step:int = 1;
		if(offset <= 0) 
		{
			start = size - 1;
			end = -1;
			step = -1;
		}
		for(let i = start; i != end; i += step) 
		{
			// i < offset: moving south, looking at north row
			// i > md + offset: moving north, looking at south row
			// dx < 0 && (i % md) == (md - 1): moving west, looking at east row
			// dx > 0 && (i % md) == 0: moving east, looking at west row
			if(i < offset || 
				i >= size + offset || 
				(dx < 0 && (i % width) == (width - 1)) ||
				(dx > 0 && (i % width) == 0))
			{
				if(chunks[i]!=null) 
				{
					//chunks[i].GetComponent(MeshFilter).mesh = null; 
					//chunks[i].GetComponent(MeshCollider).sharedMesh = null; 
					Destroy(chunks[i].gameObject); chunks[i]=null;
				}

			}
			if(i < -offset || 
				i >= size - offset || 
				(dx < 0 && (i % width) == 0) ||
				(dx > 0 && (i % width) == (width - 1)))
			{
				chunks[i] = null;
			}
			else
			{
				chunks[i] = chunks[i + offset]; 
			}
		}
		centerPos += Vector2(dx*chunkSize.x,dy*chunkSize.z);
		radius = 0;
		System.GC.Collect();
		Resources.UnloadUnusedAssets();
	}



	function canGen() :boolean 
	{
		if (lastFrame<Time.frameCount||crc!=0) 
		{
			if (lastFrame<Time.frameCount) 
			{
				lastFrame=Time.frameCount+buff;
				crc=chunksPerFrame;
			}
			else
			{
				crc--;
			}
			return true;
		} 
		else 
		{
			return false;
		}
	}


	function OnGUI () 
	{
		if (!seedSelected) 
		{
			seedString = GUI.TextField (Rect (Screen.width/2, 0, Screen.width/2/*-150*/, 30), seedString,9);
			//text = GUI.TextArea (Rect (0, 30, Screen.width, Screen.height-30), text);
			if (GUI.Button (Rect (0,0,Screen.width/2,30), "Generate")) 
			{
				if (seedString!='')seed = int.Parse(seedString);
				else
					seed=Random.Range(-10000,10000);
				//Debug.Log(seed);
				chunks = new Transform[((loop*2)*(loop*2))];

				seedSelected = true;
				let tx:int=0;

				gen.seed=seed;
				while (gen.getHeight(tx,0)<startHeight) 
				{
					tx+=chunkSize.x;
				}
				let sHeight:float = gen.getHeight(tx,0)+15;
				if (gen.blockIndex.topDown)
				{
					sHeight=1.5;
				}
				player.transform.position = Vector3(tx,sHeight,0);
				centerPos.x=tx;
			}
		}
		/*if (GUI.Button (Rect (Screen.width-150,0,150,30), "quit")) 
	{
		Application.Quit();
	}*/
		//GUI.Label(Rect (0,50,Screen.width,50),""+radius);
	}
}
