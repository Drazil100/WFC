import { seed, showDebug, showGuesses } from './config.js';

const colors = [];
for (let i = 0; i < 256; i++) {
	colors.push(`rgb(${i}, ${i}, ${i})`);
}

export const DELTA = [
	[0, -1],
	[-1, 0],
	[+1, 0],
	[0, +1],
];

function shuffleArray(x, y, array) {
	for (let i = array.length - 1; i > 0; i--) {
		// Generate random number
		const j = ((hashFunction(x, y, i + seed) / 0x7FFFFFFF) * (i + 1))|0;

		const temp = array[i];
		array[i] = array[j];
		array[j] = temp;
	}

	return array;
}

class Superposition
{
	constructor(x, y, count) {
		this.data = [];
		for (let i = 0; i < count; i++)
		{
			this.data.push(i);
		}
		shuffleArray(x, y, this.data);
		this._order = new Array(count);
		for (let i = 0; i < count; i++) {
			this._order[this.data[i]] = i;
		}
	}

	has(n) {
		return this.data[this._order[n]] >= 0;
	}

	toggle(n) {
		this.data[this._order[n]] = -1 - this.data[this._order[n]];
	}

	getAll() {
		return this.data.filter(Superposition._filter);
	}

	getFirst() {
		return this.data.find(Superposition._filter);
	}

	static _filter(n) {
		return n >= 0;
	}
}

class Tile
{
	index = 0;
	conflict = false;
	lastID = null;
	error = false;
	deps = new Set();
	constructor(x, y, count)
	{
		this.x = x;
		this.y = y;
		this.length = count;
		this.state = [null];
		this.superposition = new Superposition(x, y, count);
		this.changeHistory = new Map();
		this.isDirty = false;
	}

	getSuperposition()
	{
		return this.state[0] === null ? this.superposition.getAll() : this.state;
	}

	getFirstSuperposition()
	{
		return this.state[0] === null ? this.superposition.getFirst() : this.state[0];
	}

	setState(newState)
	{
		this.isDirty = this.state[0] !== newState;
		this.state[0] = newState;
	}

	getChangeHistory(revertID)
	{
		let revert = this.changeHistory.get(revertID);
		if (!revert) {
			revert = new Set();
			this.changeHistory.set(revertID, revert);
		}
		return revert;
	}

	guess(revertID, tileSet, index)
	{
		if (index >= this.length)
		{
			return 0;
		}
		this.isGuess = true;
		const sp = this.getSuperposition();
		this.setState(sp[index]);
		let revert = this.getChangeHistory(revertID);
		for (const pos of sp)
		{
			if (this.state[0] != pos)
			{
				this.superposition.toggle(pos);
				revert.add(pos);
				this.length--;
			}
		}
		return 1;
	}

	collapse(revertID, tileSet, direction = null, superposition = null)
	{
		if (this.lastID != revertID)
		{
			this.conflict = false;
			this.lastID = revertID;
		}
		const valid = tileSet.getValid(superposition, direction);
		let revert = this.getChangeHistory(revertID);
		let changed = false;
		for (const pos of this.getSuperposition())
		{
			if (!valid.has(pos))
			{
				this.superposition.toggle(pos);
				revert.add(pos);
				this.length--;
				changed = true;
			}
		}

		if (!changed)
		{
			return 2;
		}

		const l = this.length;
		if (l == 0)
		{
			this.deps.clear();
			for (const [key, value] of this.changeHistory)
			{
				for (const pos of valid)
				{
					if (value.has(pos))
					{
						this.deps.add(key);
					}
				}
			}
			this.conflict = true;
			this.revert(revertID);
			return 0;
		}

		//this.conflict = false;
		if (l == 1)
		{
			this.setState(this.getFirstSuperposition());
		}
		return 1;
	}

	revert(revertID)
	{
		this.isGuess = false;
		const ch = this.changeHistory.get(revertID);
		if (!ch)
		{
			throw Error(`No change history: x${this.x}, y${this.y}`);
		}
		if (ch.size == 0) return;
		this.state[0] = null;
		this.isDirty = false;
		for (const pos of this.superposition.data)
		{
			if (ch.has(-1 - pos))
			{
				this.superposition.toggle(-1 - pos);
				this.length++;
			}
		}
		ch.clear();
		//this.conflict = false
	}
}

const blankRow = [];
export default class Chunk
{
	queued = [];
	processed = [];
	showProcessed = true;
	constructor(x, y, tileSize, width, height, tileSet)
	{
		this.tiles         = this.generate(width, height, tileSize, tileSet.length);
		this.tileSize      = tileSize;
		this.x             = x;
		this.y             = y;
		this.width         = width;
		this.height        = height;
		this.tileSet       = tileSet;
		this.canvas        = document.createElement('canvas');
		this.context       = this.canvas.getContext('2d');
		this.canvas.width  = tileSize * width;
		this.canvas.height = tileSize * height;

		// Extremely tiny micro-optimization: putting this here instead of in a
		// method saves two `this.` lookups.
		this.outOfBounds = (x, y) => x < 0 || y < 0 || x >= width || y >= height;
	}

	tileAt(x, y, direction)
	{
		if (direction !== null) {
			const d = DELTA[direction];
			x += DELTA[direction][0];
			y += DELTA[direction][1];
		}
		return (this.tiles[y] || blankRow)[x];
	}

	generate(width, height, tileSize, superpositions)
	{
		const tiles = [];
		for (let i = 0; i < height; i++)
		{
			const row = [];
			tiles.push(row);
			for (let j = 0; j < width; j++)
			{
				const tile = new Tile(j, i, superpositions);
				tile.cx = Math.abs(j / width - .5);
				tile.cy = Math.abs(i / height - .5);
				tile.radius = 1 + Math.sqrt(tile.cx * tile.cx + tile.cy * tile.cy);
				row.push(tile);
			}
		}
		return tiles;

	}

	render()
	{
		if (showDebug)
		{
			for (const tile of this.queued)
			{
				if (tile.length == 1) continue;
				this.context.fillStyle = `#00FF003F`;
				this.context.fillRect(tile.x * this.tileSize, tile.y * this.tileSize, this.tileSize, this.tileSize);
			}

		}
		this.queued = [];
		let lastColor = null;
		for (const tile of this.processed)
		{
			if (tile.isDirty)
			{
				this.context.drawImage(this.tileSet._tiles[tile.getFirstSuperposition()].canvas, tile.x * this.tileSize, tile.y * this.tileSize);
				tile.isDirty = false;
				if (showGuesses && tile.isGuess)
				{
					this.context.fillStyle = `#0000FF55`;
					this.context.fillRect(tile.x * this.tileSize, tile.y * this.tileSize, this.tileSize, this.tileSize);
				}
			}
			else if (this.showProcessed)
			{
				if (tile.length == 1) continue;
				const color = Math.floor((1 - (tile.length / this.tileSet.length)) * 255);
				if (color != lastColor) {
					this.context.fillStyle = colors[color];
				}
				this.context.fillRect(tile.x * this.tileSize, tile.y * this.tileSize, this.tileSize, this.tileSize);
			}
			if (showDebug && tile.conflict)
			{
				this.context.fillStyle = `#FFFF0077`;
				this.context.fillRect(tile.x * this.tileSize, tile.y * this.tileSize, this.tileSize, this.tileSize);
			}
			if (showDebug && tile.error)
			{
				this.context.fillStyle = `#FF000055`;
				this.context.fillRect(tile.x * this.tileSize, tile.y * this.tileSize, this.tileSize, this.tileSize);
			}
		}
		this.processed = [];

		return this.canvas;
	}

	guess(revertID, x, y, index)
	{
		const tile = this.tiles[y][x];
		this.processed.push(tile);
		return tile.guess(revertID, this.tileSet, index);
	}

	collapse(revertID, x, y, direction = null)
	{
		if (direction == null)
		{
			this.processed.push(this.tiles[y][x]);
			return this.tiles[y][x].collapse(revertID, this.tileSet);
		}

		const nextTile = this.tileAt(x, y, direction);
		if (!nextTile) {
			return 2;
		}

		this.processed.push(nextTile);
		return nextTile.collapse(revertID, this.tileSet, direction, this.tiles[y][x].getSuperposition());
	}

	revert(revertID, x, y, direction = null)
	{
		const tile = this.tileAt(x, y, direction);
		if (!tile) {
			return;
		}
		this.processed.push(tile);
		tile.revert(revertID);
	}

	queue(x, y, direction = null)
	{
		const tile = this.tileAt(x, y, direction);
		if (tile) {
			this.queued.push(tile);
		}
	}

	markForError(x, y)
	{
		this.tiles[y][x].error = true;
	}

	getSmallest()
	{
		let smallestScore = Infinity;
		let smallest = null;
		let smallestX = Infinity;
		let smallestY = Infinity;

		for (const row of this.tiles) {
			for (const tile of row) {
				if (tile.length < 2) continue;
				let score = (tile.length * tile.length); //  * tile.radius);
				let adj = 0;
				if (tile.x > 0 && row[tile.x - 1].state[0] !== null) adj++;
				if (tile.y > 0 && this.tiles[tile.y - 1][tile.x].state[0] !== null) adj++;
				if (tile.x < this.width - 1 && row[tile.x + 1].state[0] !== null) adj++;
				if (tile.y < this.height - 1 && this.tiles[tile.y + 1][tile.x].state[0] !== null) adj++;
				score -= adj;
				if (score < smallestScore) {
					smallest = tile;
					smallestScore = score;
					smallestX = tile.cx;
					smallestY = tile.cy;
				} else if (tile.cx < smallestX || (tile.cx == smallestX && tile.cy < smallestY)) {
					smallest = tile;
					smallestX = tile.cx;
					smallestY = tile.cy;
				}
			}
		}
		return smallest;
	}

	getDependencies(x, y, direction = null)
	{
		return this.tileAt(x, y, direction).deps;
	}

}

function hashFunction(x, y, seedToUse)
{
	// based on Jenkins96 hash
	let a = 0x9e3779b9 + x;
	let b = 0x9e3779b9 + y;
	let c = seedToUse;

	a -= b; a -= c; a ^= (c>>13);
	b -= c; b -= a; b ^= (a<<8);
	c -= a; c -= b; c ^= (b>>13);
	a -= b; a -= c; a ^= (c>>12);
	b -= c; b -= a; b ^= (a<<16);
	c -= a; c -= b; c ^= (b>>5);
	a -= b; a -= c; a ^= (c>>3);
	b -= c; b -= a; b ^= (a<<10);
	c -= a; c -= b; c ^= (b>>15);

	c += 12;

	a -= b; a -= c; a ^= (c>>13);
	b -= c; b -= a; b ^= (a<<8);
	c -= a; c -= b; c ^= (b>>13);
	a -= b; a -= c; a ^= (c>>12);
	b -= c; b -= a; b ^= (a<<16);
	c -= a; c -= b; c ^= (b>>5);
	a -= b; a -= c; a ^= (c>>3);
	b -= c; b -= a; b ^= (a<<10);
	c -= a; c -= b; c ^= (b>>15);

	return c & 0x7FFFFFFF;
}
