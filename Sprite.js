
export class ImgInfo
{
	static _imgCache = {};
	initialized = false;
	_img = null;
	_name = "";
	_canvas = null;
	_xOffset = 0;
	_yOffset = 0;

	constructor(img, name, xOffset = 0, yOffset = 0, width = null, height = null)
	{
		this._img = img;
		this._name = name;
		this._canvas = document.createElement('canvas');
		this._xOffset = xOffset;
		this._yOffset = yOffset;
		this._canvas.width = !width ? img.naturalWidth : width;
		this._canvas.height = !height ? img.naturalHeight : height;
		if (!(this._name in ImgInfo._imgCache))
		{
			ImgInfo._imgCache[this._name] = this;
			console.log(name);
		}
		else
		{
			throw new Error('Duplicate image name: ' + this._name);
		}
	}

	get canvas()
	{
		if (!this.initialized)
		{
			this.initialize();
		}
		return this._canvas;
	}
	get name() { return this._name; }
	get width() { return this._canvas.width; }
	get height() { return this._canvas.height; }

	initialize()
	{
		if (!this.initialized)
		{
			this._canvas.getContext('2d').drawImage(this._img, this._xOffset, this._yOffset, this._canvas.width, this._canvas.height, 0, 0, this._canvas.width, this._canvas.height);
			this.initialized = true;
		}
	}

	static getImage(name)
	{
		if (name in ImgInfo._imgCache)
		{
			return ImgInfo._imgCache[name];
		}
		else
		{
			throw new ReferenceError('Image does not exist: ' + name);
		}
	}
}

export default class Sprite
{
	constructor(imgName, x, y, xAnchor = 0, yAnchor = 0, width = null, height = null) {
		this._img = imgName;
		this._x = x;
		this._y = y;
		this._ax = xAnchor;
		this._ay = yAnchor;
		this._w = !width  ? this._img.width : width;
		this._h = !height ? this._img.height : height;
	}

	get l()  { return this._x - this._ax; }
	get t()  { return this._y - this._ay; }
	get r()  { return this._x - this._ax + this._w; }
	get b()  { return this._y - this._ay + this._h; }
	get x()  { return this._x; }
	set x(v) { this._x = v; }
	get y()  { return this._y; }
	set y(v) { this._y = v; }
	get width()  { return this._w; }
	set width(v) { this._w = v; }
	get height()  { return this._h; }
	set height(v) { this._h = v; }

	translate(x, y)
	{
		this._x += x;
		this._y += y
	}

	render(ctx, cam)
	{
		/*
		if (!this._img.initialized)
		{
			this._img.initialize();
		}
		*/
		const { t, l, r, b } = this;
		if (b > -cam.y && r > -cam.x && t < -cam.y + cam.height && l < -cam.x + cam.width) {
			ctx.drawImage(this._img, l + cam.x, t + cam.y, this.width, this.height);
		}
	}

}
