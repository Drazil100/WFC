import { DELTA } from './Chunk.js';
import { autoReload } from './config.js';

const RECOVER = Symbol('recover');
const REVERT = Symbol('revert');
const CONFLICT = Symbol('conflict');
const COLLAPSE = Symbol('collapse');

window.didRollBack = false;

export default class Generator
{
	tasksRun = 0;
	stack = [];
	isDone = false
	conflict = null;
	failHistory = [];
	constructor(chunk)
	{
		this.chunk = chunk;
		const tile = chunk.getSmallest();
		this.stack.push(new BaseFrame(chunk, tile));
	}

	get currentFrame() {
		return this.stack[this.stack.length - 1];
	}

	process()
	{
		if (this.isDone)
		{
			return;
		}

		this.tasksRun++;
		if (this.stack.length == 0)
		{
			return;
			throw Error("Could not generate anything");
		}

		let thisFrame = this.currentFrame;
		switch (thisFrame.process(this.chunk))
		{
			case 0:
				//console.log(`${this.stack[this.stack.length-1].state}: ${this.stack[this.stack.length-1].revertID}`)
				this.stack.pop();
				if (thisFrame.state != RECOVER)
				{
					const conflicts = [...thisFrame.depConflicts];
					this.conflict = conflicts[conflicts.length - 1] || conflicts[0];
					console.log(`revert: ${this.stack[this.stack.length-1].revertID}`, thisFrame.depConflicts, this.conflict);
					window.didRollBack = true;
					for (const frame of this.stack)
					{
						if (frame.revertID > this.conflict)
						{
							frame.state = RECOVER;
							//console.log(frame.revertID);
						}
						else if (frame.revertID == this.conflict)
						{
							frame.state = CONFLICT;
							this.chunk.markForError(frame.x, frame.y);
						}

					}
					return;
				}

				if (this.stack.length == 0)
				{
					return;
					throw Error("Could not generate anything");
				}
				if (this.currentFrame.state != RECOVER) this.currentFrame.state = REVERT;
				break;
			case 1:
				break;
			case 2:
				const tile = this.chunk.getSmallest();
				if (tile != null)
				{
					this.stack.push(new BaseFrame(this.chunk, tile));
				}
				else
				{
					console.log("done");
					this.isDone = true;
					if (!window.didRollBack && autoReload) window.location.reload();
				}
				break;
			case 3:
				this.stack.pop();
				this.currentFrame.state = REVERT;
		}
	}
}

class BaseFrame
{
	static idGen = 0;
	stack       = new Set();
	queue       = [];
	branchQueue = new Set();
	depConflicts = new Set();
	lastState   = "";
	state       = COLLAPSE;
	index       = 0;
	constructor(chunk, tile)
	{
		this.revertID = BaseFrame.idGen++;
		this.chunk = chunk;
		this.x = tile.x;
		this.y = tile.y;
		this.queue.push(new Frame(this.revertID, tile.x, tile.y, null, this.index++));
		this.chunk.queue(this.x, this.y);
	}

	process(chunk)
	{
		this.lastState = this.state;
		if (this.state != COLLAPSE)
		{
			for (const pos of this.stack)
			{
				this.chunk.revert(this.revertID, pos[0], pos[1]);
			}
			this.stack.clear();
			if (this.state == RECOVER) return 0;
			this.state = COLLAPSE;
			this.queue.push(new Frame(this.revertID, this.x, this.y, null, this.index++));
			this.chunk.queue(this.x, this.y);
			return 1;
		}
		else
		{
			try {
				let results = this.queue[0].process(chunk);

				this.stack.add(applyDirection(this.queue[0].x, this.queue[0].y, this.queue[0].direction));
				if (results.length > 0)
				{
					for (const i of results)
					{
						this.branchQueue.add(i);
					}
				}

				this.queue.shift();
				if (this.queue.length == 0)
				{
					for (const branch of this.branchQueue)
					{
						this.queue.push(new Frame(this.revertID, branch[0], branch[1], 0));
						this.queue.push(new Frame(this.revertID, branch[0], branch[1], 1));
						this.queue.push(new Frame(this.revertID, branch[0], branch[1], 2));
						this.queue.push(new Frame(this.revertID, branch[0], branch[1], 3));
						this.chunk.queue(branch[0], branch[1], 0);
						this.chunk.queue(branch[0], branch[1], 1);
						this.chunk.queue(branch[0], branch[1], 2);
						this.chunk.queue(branch[0], branch[1], 3);
					}
					this.branchQueue.clear();
					if (this.queue.length == 0)
					{
						return 2;
					}
				}
			} catch (err) {
				if (this.queue.length == 0)
				{
					if (this.depConflicts.size == 0) return 3;
					return 0;
				}
				let deps = this.queue[0].getDependencies(this.chunk);
				for (const dep of deps)
				{
					this.depConflicts.add(dep);
				}
				if (this.stack.size == 0)
				{
					if (this.depConflicts.size == 0) return 3;
					return 0;
				}
				this.queue = [];
				this.branchQueue.clear();
				this.state = REVERT;
			}
			return 1;
		}
	}
}

const OTHER_DIRECTIONS = [
	[1, 2, 3],
	[0, 2, 3],
	[0, 1, 3],
	[0, 1, 2],
];

class Frame
{
	constructor(revertID, x, y, direction = null, index = null)
	{
		this.revertID = revertID;
		this.x = x;
		this.y = y;
		this.index = index
		this.direction = direction;
		this.directions = direction == null ? [0, 1, 2, 3] : OTHER_DIRECTIONS[direction];
	}

	process(chunk)
	{
		let result;
		if (this.direction != null)
		{
			result = chunk.collapse(this.revertID, this.x, this.y, this.direction);
		}
		else
		{
			result = chunk.guess(this.revertID, this.x, this.y, this.index);
		}

		if (!result)
		{
			throw new Error(`No options remaining: x${this.x}, y${this.y}`);
		}
		else if (result == 1)
		{
			return [applyDirection(this.x, this.y, this.direction)];
		}

		return [];
	}

	getDependencies(chunk)
	{
		return chunk.getDependencies(this.x, this.y, this.direction);
	}
}

function applyDirection(x, y, direction)
{
	if (direction == null) {
		return [x, y];
	}
	const [dx, dy] = DELTA[direction];
	return [x + dx, y + dy];
}
