const DIRS = ['up', 'left', 'right', 'down'];

class Tile
{
	constructor(img, canvas, base64Image = "")
	{
		this.img = img;
		this.canvas = canvas;
		this.base64Image = base64Image;
	}
}
class Rule
{
	constructor(x, y)
	{
		this.x = x;
		this.y = y;
		this.up = new Set();
		this.down = new Set();
		this.left = new Set();
		this.right = new Set();
	}
}

function serializeSet(set)
{
	const array = [...set];
	array.sort((a, b) => a - b);
	return array;
}

export default class TileSheet
{
	_tiles = [];
	_rules = [];
	_grid = [];
	_img = null;
	_size = 0;
	_width = 0;
	_height = 0;

	constructor(img, tileSize, rules = null)
	{
		this._img = img;
		this._size = tileSize;
		let canvas = document.createElement('canvas');
		canvas.width = tileSize;
		canvas.height = tileSize;

		if (rules != null) console.log(rules);
		if (rules != null && rules.src == img.getAttribute("src"))
		{
			this._rules = [];
			for (const rule of rules.rules)
			{
				canvas = document.createElement('canvas');
				canvas.width = tileSize;
				canvas.height = tileSize;
				canvas.getContext('2d').drawImage(img, rule.x * tileSize, rule.y * tileSize, tileSize, tileSize, 0, 0, tileSize, tileSize);
				this._tiles.push(new Tile(img, canvas));
				const newRule = new Rule(rule.x, rule.y);
				newRule.up = new Set(rule.up);
				newRule.left = new Set(rule.left);
				newRule.right = new Set(rule.right);
				newRule.down = new Set(rule.down);
				this._rules.push(newRule);
			}
			return;
		}

		this._height = (img.naturalHeight / tileSize)|0;
		this._width = (img.naturalWidth / tileSize)|0;
		for (let i = 0; i < this._height; i++)
		{
			const row = [];
			this._grid.push(row);
			for (let j = 0; j < this._width; j++)
			{
				canvas.getContext('2d').drawImage(img, j * tileSize, i * tileSize, tileSize, tileSize, 0, 0, tileSize, tileSize);
				let base64Image = this.getBase64Image(canvas);
				let isMatch = false;
				for (let k = 0; k < this._tiles.length; k++)
				{
					if (base64Image == this._tiles[k].base64Image)
					{
						isMatch = true;
						row.push(k);
						break;
					}
				}
				if (!isMatch)
				{
					this._tiles.push(new Tile(img, canvas, base64Image));
					this._rules.push(new Rule(j, i));
					row.push(this._tiles.length - 1);
					canvas = document.createElement('canvas');
					canvas.width = tileSize;
					canvas.height = tileSize;
					console.log(this._tiles.length - 1);
				}
			}
		}
		//console.log(this._grid);
		for (let i = 0; i < this._height; i++)
		{
			for (let j = 0; j < this._width; j++)
			{
				const rule = this._rules[this._grid[i][j]];
				if (i > 0)
				{
					rule.up.add(this._grid[i-1][j]);
				}
				if (i < this._grid.length - 1)
				{
					rule.down.add(this._grid[i+1][j]);
				}
				if (j > 0)
				{
					rule.left.add(this._grid[i][j-1]);
				}
				if (j < this._grid[0].length - 1)
				{
					rule.right.add(this._grid[i][j+1]);
				}
			}
		}
		const formatted = {
			src: img.getAttribute('src'),
			rules: this._rules.map(rule => ({
				x: rule.x,
				y: rule.y,
				up: serializeSet(rule.up),
				left: serializeSet(rule.left),
				right: serializeSet(rule.right),
				down: serializeSet(rule.down),
			})),
		};
		console.log(JSON.stringify(formatted));
	}

	get length() { return this._tiles.length; }

	getValid(tiles, direction)
	{
		const r = new Set();
		const d = DIRS[direction];
		for (const tile of tiles)
		{
			for (const t of this._rules[tile][d]) {
				r.add(t);
			}
		}
		return r;
	}


	getBase64Image(img)
	{
		// Create an empty canvas element
		//var canvas = document.createElement("canvas");
		//canvas.width = img.width;
		//canvas.height = img.height;

		// Copy the image contents to the canvas
		//var ctx = canvas.getContext("2d");
		//ctx.drawImage(img, 0, 0);

		// Get the data-URL formatted image
		// Firefox supports PNG and JPEG. You could check img.src to
		// guess the original format, but be aware the using "image/jpg"
		// will re-encode the image.
		var dataURL = img.toDataURL("image/png");

		return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
	}
}

