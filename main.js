import { seed, slow, pause, showDebug, chunkWidth, chunkHeight} from './config.js';
import Sprite, { ImgInfo } from './Sprite.js';
import TileSheet from './tileSheet.js';
import Chunk from './Chunk.js';
import Generator from './Generator.js';
import { tileset, keyMap } from './config.js';

const canvas = document.getElementById('giveMeAName'); // or document.querySelector('#giveMeAName')
const ctx = canvas.getContext('2d');
const tileSize = 16;
let lowfps = Infinity;
let highfps = 0;
let fps = 0;

let fpsDebug = "";
let lastTimestamp = 0;
let frameRate = 1000/60;
let currentFrame = 0;
let lastFrame = 0;
let startTime;
let deltaTime = 0;

let input = { up: 0, down: 0, left: 0, right: 0, touchStartX: null, touchStartY: null, increaseSpeed: 0, decreaseSpeed:0, resetSpeed: 0, x: 0, y: 0 };
let paused = pause;
let speedDivisor = 1;
let chunk = null;
let generator = null;
let camera = { x: 0, y: 0, width: canvas.width, height: canvas.height };

let sprites = [];


function getViewBounds() {
	const transform = ctx.getTransform();
	return {x: -transform.e, y: -transform.f, w: canvas.width, h: canvas.height};
}

function loadImage(url) {
	return new Promise(resolve => {
		const img = document.createElement('IMG');
		img.src = url;
		img.addEventListener('load', () => resolve(img));
	});
}

function getJSON(url) {
	return fetch(url).then(response => response.json());
}

async function loadImages() {
	const promises = [];
	promises.push(loadImage(`data/${tileset}.png`));
	promises.push(getJSON(`data/${tileset}.json`).catch(() => null));
	return await Promise.all(promises);
}


function mainLoop(time) {
	deltaTime = time - lastTimestamp;
	fps = 1000/deltaTime;
	lastTimestamp = time;

	if (fps < lowfps)
		lowfps = fps;
	if (fps > highfps)
		highfps = fps;
	fpsDebug = `Low: ${lowfps.toFixed(2)}, high: ${highfps.toFixed(2)}, fps: ${fps.toFixed(2)}`;
	canvas.width = window.innerWidth;
	canvas.height = window.innerHeight;
	camera.width = canvas.width;
	camera.height = canvas.height;
	ctx.clearRect(0, 0, camera.width, camera.height);
	ctx.fillStyle = "#777777";
	ctx.fillRect(0, 0,  camera.width, camera.height);

	if (!paused) {
		update();
	}

	if (input.pause) {
		input.pause = 0;
		paused = !paused;
	}

	if (input.increaseSpeed) {
		input.increaseSpeed = 0;
		if (paused) {
			paused = false;
			speedDivisor = 256;
		}
		else if (speedDivisor > 1)
			speedDivisor /= 2;
	}

	if (input.decreaseSpeed) {
		input.decreaseSpeed = 0;
		if (speedDivisor == 256) {
			paused = true;
		}
		else if (speedDivisor < 256)
			speedDivisor *= 2;
	}

	if (input.resetSpeed) {
		speedDivisor = 1;
		input.resetSpeed = 0;
	}
	updateWhilePaused();
	requestAnimationFrame(mainLoop);
}

let perfNum = 0;
let perfDenom = 0;
let frameCount = 0;
function update() {
	if (!generator.isDone) {
		const start = performance.now();
		let i = 0;
		let t = null;
		do {
			if (speedDivisor <= 16 || frameCount % (speedDivisor/16) == 0)
			{
				generator.process();
			}
			++i;
			t = performance.now();
		} while (t - start < (15 / speedDivisor) && !slow);
		perfNum += t - start;
		perfDenom += i;
		frameCount++;
	}
	chunk.render();

}

function updateWhilePaused()
{
	camera.x += 0.4 * input.x * deltaTime;
	camera.y += 0.4 * input.y * deltaTime;
	ctx.drawImage(chunk.render(), 0, 0, chunk.width * tileSize, chunk.height * tileSize, camera.x, camera.y, chunk.width * tileSize*1, chunk.height *tileSize*1);
	try
	{
		fpsDebug = `speed: ${paused ? "paused" : ((speedDivisor == 1) ? "1x" : '1/' + speedDivisor + "x")}, ${fpsDebug}, tasks: ${generator.tasksRun}, stacks: ${generator.stack.length}, revertID: ${generator.stack[generator.stack.length-1].revertID}, state: ${generator.stack[generator.stack.length-1].state.description} perf: ${(1000 * perfNum / (perfDenom || 1)).toFixed(2)} ms/iter, ${perfNum|0} ms, ${perfDenom} iter, ${frameCount} frames`;
	}
	catch (e)
	{
		fpsDebug = e.message;
	}
	ctx.fillStyle = "#ffffffDD";
	ctx.fillRect(0, 0,  ctx.measureText(fpsDebug).width+10, 18);

	ctx.fillStyle = 'black';
	ctx.fillText(fpsDebug, 5, 14);

	ctx.fillStyle = "#ffffffDD";
	ctx.fillRect(camera.width - ctx.measureText("download").width-10, 0,  ctx.measureText(download).width+2, 18);
	ctx.fillStyle = 'blue';
	ctx.fillText("download", camera.width - ctx.measureText("download").width-5, 14);
}

async function start() {
	console.log(`seed: ${seed}`);
	const [png, json] = await loadImages();
	ctx.fillStyle = 'black';
	ctx.font = "20px serif"

	let tiles = new TileSheet(png, tileSize, json);

	chunk = new Chunk(0, 0, tileSize, chunkWidth, chunkHeight, tiles);
	generator = new Generator(chunk);
	requestAnimationFrame(mainLoop);
}

async function download(filename, img) {
	const url = URL.createObjectURL(await new Promise(resolve => img.toBlob(resolve)));
	const element = document.createElement('a');
	element.setAttribute('href', url);
	element.setAttribute('download', filename);

	element.style.display = 'none';
	document.body.appendChild(element);

	element.click();

	document.body.removeChild(element);
	URL.revokeObjectURL(url);
}

function onKeyDown(e) {
	const mapping = keyMap[e.key];
	if (mapping) {
		e.preventDefault();
		input[mapping] = 1;
		input.x = input.left - input.right;
		input.y = input.up - input.down;
	}
	//console.log(e.key, input);
}

function onKeyUp(e) {
	const mapping = keyMap[e.key];
	if (mapping) {
		e.preventDefault();
		input[mapping] = 0;
		input.x = input.left - input.right;
		input.y = input.up - input.down;
	}
}

function printMousePos(event)
{
	if (event.clientX >= camera.width - ctx.measureText("download").width-10 && event.clientY <= 18)
	{
		download(`${tileset}_${seed}.png`, chunk.render());
		return;
	}
	let x = Math.floor(event.clientX - camera.x);
	let y = Math.floor(event.clientY - camera.y);
	x = (x - (x % tileSize)) / tileSize;
	y = (y - (y % tileSize)) / tileSize;
	console.log(` x: ${x}, y: ${y}, superposition: [${chunk.tiles[y][x].getSuperposition().join(', ')}]`);
	//console.log(chunk.tiles[y][x]);
}

function handleTouchEvent(e) {
	if (e.touches.length === 0 ) 
	{
		input.touchStartX = null;
		input.touchStartY = null;

		return;
	}
	e.preventDefault();
	e.stopPropagation();

	let touch = e.touches[0];

	if (input.touchStartX == null)
	{
		
		input.touchStartX = camera.x - touch.pageX;
		input.touchStartY = camera.y - touch.pageY;
		if (touch.pageX >= camera.width - ctx.measureText("download").width-10 && touch.pageY <= 18)
		{
			download(`${tileset}_${seed}.png`, chunk.render());
			return;
		}

	}
	camera.x = input.touchStartX + touch.pageX;
	camera.y = input.touchStartY + touch.pageY;

}

document.addEventListener("click", printMousePos);
window.addEventListener('keydown', onKeyDown);
window.addEventListener('keyup', onKeyUp);
window.addEventListener('load', start);
// listen any touch event
canvas.addEventListener('touchstart', handleTouchEvent, true);
canvas.addEventListener('touchmove', handleTouchEvent, true);
canvas.addEventListener('touchend', handleTouchEvent, true);
canvas.addEventListener('touchcancel', handleTouchEvent, true);

